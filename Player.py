
class Player():
	def __init__(self):
		self.step = -1
		# path is the path, in order, to find the meaning of life
		self.path = ['GET ALL', 'OPEN DOOR', 'EAST', 'GET EDELWEISS', 'UP', 'ENTER CAVE', 'LIGHT FIRE', 'WAIT', 'PUT EDELWEISS IN FIRE',
						'PUT HELMET IN STATUE', 'PUT PRISM IN PICKLE', 'EXIT CAVE', 'NORTH', 'GET MEANING OF LIFE']

		# verbs is a dict with the verb as the key
		# value is a tuple of #args, function, and string displayed with 'help' is entered
		self.verbs = {	'LIGHT':   (2, self.light, "LIGHT x"),		'NORTH':	(1, self.north, "NORTH"),
						'EXAMINE':(0, self.examine, "EXAMINE x"),	'SOUTH':	(1, self.south, "SOUTH"),
						'DROP':	  (2, self.drop, "DROP x"),			'WEST':		(1, self.west, "WEST"),
						'PUT':    (4, self.put, "PUT x IN y"),		'EAST':		(1, self.east, "EAST"),
						'WAIT':   (1, self.wait, "WAIT"),			'UP':		(1, self.up, "UP"),
						'ENTER':  (2, self.enter, "ENTER x"),		'DOWN':		(1, self.down, "DOWN"),
						'GET':    (0, self.get, "GET ..."),			'TAKE':		(0, self.get, "[ See GET usage ]"),
						'EXIT':   (2, self.exit, "EXIT x"),			'HELP':		(1, self.help, "HELP"),
						'OPEN':   (2, self.open, "OPEN x"),			'HINT':		(1, self.hint, "HINT") }

		# hints is a dict of possible actions for a particular step
		self.hints = {'zero':	['GET ALL', 'TAKE ALL', 'OPEN DOOR', 'WAIT'],
						'one':		['EXIT ROOM', 'OPEN DOOR', 'EXAMINE HELMET', 'WAIT'],
						'two':		['NORTH', 'EAST', 'SOUTH', 'WEST', 'WAIT'],
						'three':	['GET EDELWEISS', 'LIGHT EDELWEISS', 'DROP HELMET', 'WAIT', 'NORTH'],
						'four':		['WAIT', 'UP', 'DROP PRISM'],
						'five':		['ENTER CAVE', 'WAIT', 'DOWN'],
						'six':		['WAIT', 'LIGHT FIRE', 'EXIT CAVE'],
						'seven':	['WAIT', 'LIGHT EDELWEISS', 'EXIT CAVE'],
						'eight':	['PUT EDELWEISS IN FIRE', 'PUT PRISM IN FIRE', 'PUT HELMET IN FIRE'],
						'nine':		['PUT PRISM IN STATUE', 'PUT HELMET IN STATUE', 'WAIT'],
						'ten':		['PUT PRISM IN PICKLE', 'WAIT', 'EXAMINE PICKLE'],
						'eleven':	['EXIT CAVE', 'WAIT', 'EXAMINE ROCK'],
						'twelve':	['NORTH', 'EAST', 'WEST', 'WAIT'],
						'thirteen':	['GET MEANING OF LIFE', 'EXAMINE MEANING OF LIFE', 'SOUTH']}

	def wait(self):
		print("You wait")
	def north(self):
		print("You attempt to go NORTH")
	def south(self):
		print("You attempt to go SOUTH")
	def east(self):
		print("You attempt to go EAST")
	def west(self):
		print("You attempt to go WEST")
	def up(self):
		print("You attempt to go UP")
	def down(self):
		print("You attempt to go DOWN")

	def put(self, thing1, thing2):
		print("You attempt to put", thing1, "in", thing2)

	def light(self, thing):
		print("You attempt to light", thing)
	def open(self, thing):
		print("You attempt to open", thing)
	def enter(self, place):
		print("You attempt to enter", place)
	def exit(self, place):
		print("You attempt to exit", place)
	def drop(self, thing):
		print("You attempt to drop", thing)

	def examine(self, args):
		""" Method takes variable number of words and prints """
		print("You attempt to examine", end=' ')
		for thing in args:
			print(thing, end=' ')
		print()
	def get(self, args):
		print("You attempt to get", end=' ')
		for thing in args:
			print(thing, end=' ')
		print()

	def help(self):
		""" Method displays all the verbs and example usage """
		print('{0:10} {1:20}'.format("VERB", "USAGE"))
		print('-' * 30)
		for action in self.verbs.keys():
			print('{0:10} {1:20}'.format(action, self.verbs.get(action)[2]))
	def hint(self):
		""" Method displays hints based upon current step in path """
		strs = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen']
		print(self.hints.get(strs[self.step]))
