import os
import time
import Player


def main():
	""" Main function holds the game loop and the print statements upon winning """
	p1 = Player.Player()
	while(p1.step < 14):  # game loop
		areaPrint(p1)  # prints out the prompt for the current area
		choice = input("\nWhat do you do? >>").upper()

		if(makeChoice(p1, choice) or choice == 'HELP' or choice == 'HINT'):  # makeChoice prints out the specified action
			continue

		if(wasSuccess(p1, choice)):  # wasSuccess checks to see if the input is the next correct step
			p1.step += 1
		else:  # start over if it was the incorrect input
			p1.step = 0

	print("    ----    YOU'VE ESCAPED!    ----")
	print("    ----    CONGRATULATIONS    ----")


def wasSuccess(p1, choice):
	""" Function prints out whether action was a success of fail """
	tester = choice.split()
	if(tester[0] == "TAKE"):  # if user entered TAKE, change it to GET
		choice = "GET" + choice[4:]

	if(choice == p1.path[p1.step]):  # Correct input for the current step in the path
		print("\n    ----    SUCCESS    ----\n")
		return(True)
	else:  # incorrect input
		time.sleep(2)
		os.system('clear')
		print("NULL instantly appears and your soul gets forced back into a vessel in the corner of the starting room.\n")
		return(False)


def makeChoice(p1, choice):
	""" Function takes user input and calls the corresponding Player() method """
	print()
	if(not choice):  # user just hit enter
		print("Please enter something.")
		return(1)

	userInput = choice.split()
	for word in userInput:  # loop to check for alphabetic characters only
		if(not word.isalpha()):
			print("Invalid input.")
			return(1)

	values = p1.verbs.get(userInput[0])  # get the values of the input verb
	if(values):
		if(len(userInput) == values[0] == 4):  # arguments is four so it must be the PUT verb
			p1.verbs.get(userInput[0])[1](userInput[1], userInput[3])
		elif(len(userInput) == values[0] == 2):  # input was two words
			p1.verbs.get(userInput[0])[1](userInput[1])
		elif(len(userInput) == values[0] == 1):  # input was one word
			p1.verbs.get(userInput[0])[1]()
		elif(values[0] == 0):  # input can take multiple words.
			p1.verbs.get(userInput[0])[1](userInput[1:])
		else:  # number of words entered did not match any specified formats
			print("Invalid arguments.")
			return(1)

	else:  # no verb was found
		print("Invalid action.")
		return(1)


def areaPrint(p1):
	""" Function prints out the intro and prompts for each step """
	if(p1.step == -1):  # intro prompt
		os.system('clear')
		input("Press <enter> to advance the initial introduction prompts.")
		input("Type in 'help' to see a list of commands and an example usage.")
		input("Type in 'hint' to get a couple possible actions to do.")
		os.system('clear')
		input("    ----    WELCOME TO THE JOURNEY    ----")
		input("You are an unfortunate developer who, one day, was programming a simple program when he stumbled across NULL.")
		input("NULL immediately disguised himself as a shortcut and lured your soul into a dreamlike world in which only the discovery of the meaning of life can be the only escape.")
		os.system('clear')
		print("You look around and see a PRISM, and a HELMET to your left and right. There is a DOOR on the opposite side of the room.")
		p1.step += 1
	elif(p1.step == 0):  # next verb: get all
		print("You look around and see a PRISM, and a HELMET to your left and right. There is a DOOR on the opposite side of the room.")
	elif(p1.step == 1):  # next verb: open door
		print("There is nothing else in the room but the DOOR on the opposite side of the room.")
	elif(p1.step == 2):  # next verb: east
		print("As you open the DOOR and travel through it, the world rotates 90 degrees backwards and you are now standing on the door.")
	elif(p1.step == 3):  # next verb: get edelweiss
		print("On your travels, you come across a weird looking EDELWEISS (a flower) on the side of the path.")
	elif(p1.step == 4):  # next verb: up
		print("You turn around and there is a ledge that you can go UP over.")
	elif(p1.step == 5):  # next verb: enter cave
		print("You look around from your higher view and see a cliff with a fifty foot drop to your EAST and a cave right in front of you.")
	elif(p1.step == 6):  # next verb: light fire
		print("You cave is cold and wet but you see firepit with burning embers.")
	elif(p1.step == 7):  # next verb: wait
		print("The fire slowly builds.")
	elif(p1.step == 8):  # next verb: put edelweiss in fire
		print("As the fire gets larger, you feel the EDELWEISS in your pocket start to gravitate more toward the FIRE.")
	elif(p1.step == 9):  # next verb: put helmet in statue
		print("The EDELWEISS explodes into a huge cloud of smoke and when it clears, there is a statue that has a huge opening with the words 'Require Headdress' inscribed on it.")
	elif(p1.step == 10):  # next verb: put prism in pickle
		print("You fall through the floor and land on a bunch of bones. You see the end of a giant PICKLE open up and slowly start to slither over towards you. The PRISM in your pocket suddenly shines brighter as the PICKLE approaches.")
	elif(p1.step == 11):  # next verb: exit cave
		print("The pickle retreats into a corner. A large rock rolls onto it's side and light comes flowing into your current area.")
	elif(p1.step == 12):  # next verb: north
		print("Once you EXIT the cave, you see a bright light to the NORTH, a monster truck to the EAST, and a sleeping Pterodactyl to the WEST")
	elif(p1.step == 13):  # next verb: get meaning of life
		print("On your travel, you come to find out that the MEANING OF LIFE is what is shining so bright.")
	else:
		print("Invalid step.")


if __name__ == "__main__":
	main()
